<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Book store</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="login.css">
<style>
body {
	background-image: url('20602.jpg');
	background-size: cover;
}

form {
	border: 3px solid #f1f1f1;
	background-color: white;
	width: 30%;
	/* position: fixed; */
	margin-left: 33%;
	margin-top: 2%;
	padding: 10px;
	background: #3c3c42;
}

input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #04AA6D;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 30%;
}

button:hover {
	opacity: 0.8;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}

img.avatar {
	width: 40%;
	border-radius: 50%;
	background: white;
	padding: 18px;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.container {
	padding: 1px;
}

.alrady {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
	text-decoration: none;
}

.home {
	background-color: #f44336;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 14%;
	text-decoration: none;
	font-weight: 700;
	float: right;
}

.welcome {
	bottom: 0%;
	left: 0%;
	background-color: rgb(104, 9, 69);
	color: oldlace;
	position: fixed;
	min-width: 100%;
	font-size: smaller;
	padding: 0px;
	font-size: 12px;
}

.shadowbox {
	padding: 10px 10px 20px 10px;
	border: 1px solid #BFBFBF;
	box-shadow: 10px 10px 5px #aaaaaa;
	margin-bottom: 60px;
}

.loginAs {
	width: 100%;
	padding: 11px;
	margin-top: 5px;
}

#tophead {
	background-color: rgb(2, 26, 26);
	width: 100%;
	padding: 6px;
}

.heading {
	color: wheat;
	font-size: 20px;
}

.signupp {
	background-color: #04AA6D;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 7%;
	margin-left: 900px;
}

.logignup {
	background-color: #04AA6D;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 7%;
}

.accept-box {
	display: flex;
	align-items: center;
	margin: auto;
	font-size: 15px;
}
</style>
</head>
<body>
	<div id="tophead">
		<a class="heading" href="Home.html">Online Book Store</a> <a
			href="New User.html"><button class="signupp">Sign Up</button></a> <a
			href="index.html"></a>
		<button class="logignup">Login</button>
		</a>
	</div>
	<header>
		<h1 align="center" style="color: white;">Login</h1>
	</header>
	<form action="Benificiary.html" method="get" class="shadowbox">
		<div class="imgcontainer">
			<img src="https://www.nicepng.com/png/detail/263-2636180_admin-login-png-admin-login-image-png.png" alt="Avatar" class="avatar">
		</div>
		<div class="container">
			<label for="uname" style="color: white;"><b>Username</b></label> <input
				type="text" id="uname" placeholder="Enter Username" name="uname"
				required> <label for="psw" style="color: white;"><b>Password</b></label>
			<input type="password" placeholder="Enter Password" name="psw"
				id="psw" required> <label for="psw" style="color: white;"><b>Login
					As</b></label><br /> <select id="accountType" class="loginAs">
				<option selected>Admin</option>
				<option>User</option>
			</select>
		</div>
		<br />
		<div class="accept-box">
			<input type="checkbox" name="accept" id="accept" checked>
			<p>I accept the Terms & Conditions</p>
		</div>

		<button>Login</button>
		</td>
		<tr>
			<a href="New User.html" class="home">Sign Up</a>
		</tr>

		<label> </label>
		</div>

	</form>


</body>


</head>
</html>
